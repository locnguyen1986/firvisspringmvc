package com.itdragon.firvis.admin.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.itdragon.firvis.dao.CategoryDao;
import com.itdragon.firvis.dao.ImageFileDao;
import com.itdragon.firvis.dao.ProductDao;
import com.itdragon.firvis.dao.SettingsDao;
import com.itdragon.firvis.domain.Category;
import com.itdragon.firvis.domain.ImageFile;
import com.itdragon.firvis.domain.Product;
import com.itdragon.firvis.utils.Constant;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/admin/product")
public class AdminProductManagementController {

	private static final Logger logger = LoggerFactory.getLogger(AdminProductManagementController.class);

	@Autowired
	private ProductDao productDao;

	@Autowired
	private ImageFileDao imageFileDao;

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private SettingsDao settingsDao;

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String gotoProductManagement(Model model) {
		// Map categories = getCategories();
		model.addAttribute("products", productDao.getAll());
		return "admin/product/list";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String gotoEditProductPage(@RequestParam(value = "productId", required = true) Integer productId, Model model) {
		// Map categories = getCategories();
		model.addAttribute("currentProduct", productDao.getById(productId));
		return "admin/product/edit";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String editProduct(ModelMap map, HttpServletRequest request, @RequestParam("inputImageFile") MultipartFile file) {
		// Map categories = getCategories();
		logger.info("productId:" + request.getParameter("productId"));
		logger.info("productName:" + request.getParameter("productName"));
		Integer productId = Integer.parseInt(request.getParameter("productId"));
		Product product = productDao.getById(productId);

		// process only if its multipart content

		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				ArrayList<ImageFile> listImage = new ArrayList<ImageFile>();
				String imageName = Calendar.getInstance().getTimeInMillis() + "";
				saveImageToFile(file, imageName);
				ImageFile imageFile = new ImageFile();
				imageFile.setFileName(imageName);
				imageFile.setProduct(product);
				imageFile.setSmallImage(true);
				logger.info("add new image");
				imageFile = imageFileDao.create(imageFile);
				product.setSmallImage(imageFile);
				listImage.add(imageFile);
				product.setImages(listImage);
				// File uploaded successfully
				request.setAttribute("message", "File Uploaded Successfully:" + file.getSize());
			} catch (Exception ex) {
				request.setAttribute("message", "File Upload Failed due to " + ex);
			}
		} else {
			request.setAttribute("message", "File Upload Failed due to invalid file configuration");
		}

		product.setDescription(request.getParameter("editorProductDescription"));
		product.setContent(request.getParameter("editorProductContent"));
		product.setName(request.getParameter("productName"));

		productDao.update(product);
		map.addAttribute("currentProduct", productDao.getById(productId));
		return "admin/product/edit";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String gotoAddNewProductPage(Model model) {
		// Map categories = getCategories();
		// model.addAttribute("currentProduct", productDao.getById(productId));
		model.addAttribute("categories", categoryDao.getAll());
		return "admin/product/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNewProduct(ModelMap model, HttpServletRequest request, @RequestParam(value = "listCategories", required = false) String[] listCategories,
			@RequestParam("inputImageFile") MultipartFile file) {
		// Map categories = getCategories();
		// logger.info("productId:" + request.getParameter("productId"));
		logger.info("productName:" + request.getParameter("productName"));
		// Integer productId =
		// Integer.parseInt(request.getParameter("productId"));
		Product product = new Product();

		product.setDescription(request.getParameter("editorProductDescription"));
		product.setContent(request.getParameter("editorProductContent"));
		product.setName(request.getParameter("productName"));

		product = productDao.create(product);
		// process only if its multipart content

		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				ArrayList<ImageFile> listImage = new ArrayList<ImageFile>();
				String imageName = Calendar.getInstance().getTimeInMillis() + "";
				saveImageToFile(file, imageName);
				ImageFile imageFile = new ImageFile();
				imageFile.setFileName(imageName);
				imageFile.setProduct(product);
				imageFile.setSmallImage(true);
				logger.info("add new image");
				imageFile = imageFileDao.create(imageFile);
				product.setSmallImage(imageFile);
				listImage.add(imageFile);
				product.setImages(listImage);
				// File uploaded successfully
				request.setAttribute("message", "File Uploaded Successfully:" + file.getSize());
			} catch (Exception ex) {
				request.setAttribute("message", "File Upload Failed due to " + ex);
			}
		} else {
			request.setAttribute("message", "File Upload Failed due to invalid file configuration");
		}

		List<Category> listProductCategories = new ArrayList<Category>();
		logger.info("categories:" + listCategories);
		if (listCategories != null && listCategories.length > 0) {
			for (int i = 0; i < listCategories.length; i++) {
				Category category = categoryDao.getById(Integer.parseInt(listCategories[i]));
				listProductCategories.add(category);
			}
		}
		product.setCategories(listProductCategories);

		product = productDao.update(product);
		model.addAttribute("products", productDao.getAll());
		return "admin/product/list";
	}

	public void saveImageToFile(MultipartFile item, String imageName) {
		try {

			String imageLocationFromSetting = settingsDao.getSetting(Constant.IMAGE_UPLOAD_LOCATION_PROPERTY_KEY).getValue();
			logger.info("imageLocationFromSetting:" + imageLocationFromSetting);
			String imageFileLocation = imageLocationFromSetting + "\\" + imageName + ".jpg";
			logger.info("imageFileLocation:" + imageFileLocation);
			// Create the file on server
			File serverFile = new File(imageFileLocation);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(item.getBytes());
			stream.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
