package com.itdragon.firvis.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.itdragon.firvis.dao.CategoryDao;
import com.itdragon.firvis.dao.SettingsDao;
import com.itdragon.firvis.domain.Category;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/admin/category")
public class AdminCategoryManagementController {

	private static final Logger logger = LoggerFactory.getLogger(AdminCategoryManagementController.class);

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private SettingsDao settingsDao;

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String gotoCategoryManagement(Model model) {
		// Map categories = getCategories();
		model.addAttribute("categories", categoryDao.getAll());
		return "admin/category/list";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String gotoEditCategoryPage(@RequestParam(value = "categoryId", required = true) Integer categoryId, Model model) {
		// Map categories = getCategories();
		model.addAttribute("currentCategory", categoryDao.getById(categoryId));
		return "admin/category/edit";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String editCategory(ModelMap model, HttpServletRequest request) {
		try {
			// Map categories = getCategories();
			logger.info("categoryId:" + request.getParameter("categoryId"));
			logger.info("categoryName:" + request.getParameter("categoryName"));
			Integer categoryId = Integer.parseInt(request.getParameter("categoryId"));
			Category category = categoryDao.getById(categoryId);
			if (!category.getName().equals(request.getParameter("categoryName"))) {
				category.setName(request.getParameter("categoryName"));
				categoryDao.update(category);
			}

			model.addAttribute("message", "This category is edited successful");
		} catch (Exception ex) {
			ex.printStackTrace();
			model.addAttribute("message", "This category cannot be edited");
		}
		model.addAttribute("categories", categoryDao.getAll());
		return "admin/category/list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String gotoAddCategoryPage(Model model) {
		// Map categories = getCategories();
		// model.addAttribute("currentCategory",
		// categoryDao.getById(categoryId));
		return "admin/category/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNewCategory(@RequestParam(value = "categoryName", required = true) String categoryName, Model model) {
		try {
			Category category = new Category();
			category.setName(categoryName);
			categoryDao.create(category);
			// Map categories = getCategories();
			// model.addAttribute("currentCategory",
			// categoryDao.getById(categoryId));

			model.addAttribute("message", "This category is added successful");
		} catch (Exception ex) {
			ex.printStackTrace();
			model.addAttribute("message", "This category cannot be added");
		}
		model.addAttribute("categories", categoryDao.getAll());
		return "admin/category/list";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteOneCategory(@RequestParam(value = "categoryId", required = true) Integer categoryId, Model model) {
		// Map categories = getCategories();
		try {
			logger.info("categoryId:" + categoryId);
			Category category = categoryDao.getById(categoryId);
			categoryDao.delete(category);

			model.addAttribute("message", "This category is deleted successful");
		} catch (Exception ex) {
			ex.printStackTrace();
			model.addAttribute("message", "This category cannot be deleted");
		}
		model.addAttribute("categories", categoryDao.getAll());
		return "admin/category/list";
	}
}
