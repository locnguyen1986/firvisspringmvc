package com.itdragon.firvis.controller;

import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itdragon.firvis.dao.SettingsDao;
import com.itdragon.firvis.utils.Constant;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/SendEmail")
public class SendEmailController {

	private static final Logger logger = LoggerFactory.getLogger(SendEmailController.class);

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getHomePage(Model model) {
		return "index";
	}
	
	@Autowired
	private SettingsDao settingDao;

	@RequestMapping(method = RequestMethod.POST)
	public String sendEmailPage(ModelMap map, HttpServletRequest request) {
		logger.info("Start getHomePageLink" );
		Map<String, String[]> parameters = request.getParameterMap();
		for (String key : parameters.keySet()) {
			String[] vals = parameters.get(key);
			for(String val : vals)
				logger.info(key + " -> " + val);
		}
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		String toAddresses = "locnguyen1986@gmail.com";
		String emailReceiver = settingDao.getSetting(Constant.EMAIL_RECEIVER_PROPERTY_KEY).getValue();
		toAddresses = emailReceiver;
		String text = "<b>This is an email from website firvis@gmail.com</b><br> " + firstName + " " + lastName + " had sent an email to you via website contact us function <br>"
				+ "Below are sender information: <br>" + " - name: " + firstName + " " + lastName + "<br>" + " - email: " + email + "<br>" + " - address: " + address + "<br>"
				+ "and message content : <br>" + " - subject : " + subject + " <br>" + " - content: <span style='font-style: italic;'>" + message + "</span>";

		boolean isSent = sendMail(toAddresses, subject, text);
		if(isSent)
			map.addAttribute("isSent", "true");
		else 
			map.addAttribute("isSent", "false");
		logger.info("Successfully send mail to - " + toAddresses + " with content: " + text);
		return "contactus";
	}

	public static boolean sendMail(String toAddresses, String subject, String text) {
		boolean isSent = false;
		final String username = "firviswebsite@gmail.com";
		final String password = "firviswebsite123";
		final String smtpHost = "smtp.gmail.com";
		final String smtpPort = "587";
		final String emailId = "firviswebsite@gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailId));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddresses));
			message.setSubject(subject);
			message.setContent(text, "text/html");
			Transport.send(message);
			isSent = true;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return isSent;
	}

}
