package com.itdragon.firvis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/ContactUs")
public class ContactUsController {

	private static final Logger logger = LoggerFactory.getLogger(ContactUsController.class);
    
	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getContactUsPage(Model model) {
		return "contactus";
	}

}
