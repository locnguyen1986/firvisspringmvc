package com.itdragon.firvis.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import com.itdragon.firvis.dao.CategoryDao;
import com.itdragon.firvis.dao.ImageFileDao;
import com.itdragon.firvis.dao.ProductDao;
import com.itdragon.firvis.dao.SettingsDao;
import com.itdragon.firvis.domain.Category;
import com.itdragon.firvis.domain.Product;
import com.itdragon.firvis.utils.Constant;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ProductDetailController {

	


	private static final Logger logger = LoggerFactory.getLogger(ProductDetailController.class);

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private SettingsDao settingDao;

	@Autowired
	private ImageFileDao imageFileDao;

	@Autowired
	private CategoryDao categoryDao;

	@ModelAttribute("categories")
	public List<Category> getCategories() {
		logger.info("CategoryController Call getCategories");
		logger.info("ProductController Call getCategories");
		List<Category> listCategories = categoryDao.getAll();
		return listCategories;
	}

	@ModelAttribute("product")
	public Product getDefaultProduct(@RequestParam(value = "productId", required = false) Integer productId) {
		logger.info("ProductDetailController Call getCategories");
		if (productId == null)
			productId = 1;
		return productDao.getById(productId);
	}

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(value = "/Product-Detail", method = RequestMethod.GET)
	public String getProductDetailPage(Model model) {
		return "product-detail";
	}

	@RequestMapping("/GetImage/{id}")
	public void getImage(HttpServletResponse response, @PathVariable("id") final String id) throws IOException {
		response.setContentType("image/jpeg");
		// byte[] imageBytes =
		// imageFileDao.getImageContentById(Integer.parseInt(id));
		String imageLocationFromSetting = settingDao.getSetting(Constant.IMAGE_UPLOAD_LOCATION_PROPERTY_KEY).getValue();
		
		logger.info("imageLocationFromSetting:" + imageLocationFromSetting);
		String imageFileLocation = imageLocationFromSetting + "\\" + id + ".jpg";
		
		logger.info("imageFileLocation:" + imageFileLocation);
		
		BufferedImage originalImage = ImageIO.read(new File(imageFileLocation));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write( originalImage, "png", baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();		
		response.getOutputStream().write(imageInByte);
		response.getOutputStream().flush();
	}

}
