package com.itdragon.firvis.controller;

import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import com.itdragon.firvis.dao.CategoryDao;
import com.itdragon.firvis.dao.ProductDao;
import com.itdragon.firvis.dao.ProductDaoImpl;
import com.itdragon.firvis.domain.Category;
import com.itdragon.firvis.domain.Product;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ProductController  {

	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private ProductDao productDao;

	@ModelAttribute("categories")
	public List<Category> getCategories() {
		logger.info("ProductController Call getCategories");
		List<Category> listCategories = categoryDao.getAll();
		return listCategories;
	}

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(value = "/Product", method = RequestMethod.GET)
	public String getProductPage(@RequestParam(value = "categoryId", required = false) Integer categoryId, @RequestParam(value = "pagenumber", required = false) Integer pagenumber, Model model) {
		try {
			logger.info("ProductController Call getProductPage");
			if (categoryId == null)
				categoryId = 1;
			if (pagenumber == null)
				pagenumber = 0;
			logger.info("productDao.pagenumber:" + pagenumber);
			logger.info("productDao.categoryId:" + categoryId);
			int totalPage = (int) Math.ceil(productDao.getByCategoryId(categoryId).size() * 1.0 / ProductDaoImpl.PAGE_SIZE);
			if (pagenumber < 0 || pagenumber > totalPage)
				pagenumber = 0;

			model.addAttribute("products", productDao.getByCategoryId(categoryId, pagenumber));
			model.addAttribute("totalPage", totalPage);
			model.addAttribute("currentPage", pagenumber);
			model.addAttribute("categoryId", categoryId);

			return "product";
		} catch (Exception ex) {
			return "index";
		}
	}

	/**
	 * Requests to http://localhost:8080/hello will be mapped here. Everytime
	 * invoked, we pass list of all persons to view
	 */
	@RequestMapping(value = "/SearchProduct", method = RequestMethod.GET)
	public String searchProductPage(@RequestParam(value = "searchTerm", required = false) String searchTerm, @RequestParam(value = "pagenumber", required = false) Integer pagenumber, Model model) {
		try {
			logger.info("ProductController Call searchProductPage:" + searchTerm);
			if (searchTerm == null || searchTerm.isEmpty()) {
				model.addAttribute("searchError", "Invalid Search Term");
				return "index";
			}
			if (pagenumber == null)
				pagenumber = 0;
			logger.info("productDao.pagenumber:" + pagenumber);
			logger.info("productDao.searchTerm:" + searchTerm);
			int totalPage = (int) Math.ceil(productDao.getByProductBySearchTerm(searchTerm).size() * 1.0 / ProductDaoImpl.PAGE_SIZE);
			if (pagenumber < 0 || pagenumber > totalPage)
				pagenumber = 0;

			model.addAttribute("products", productDao.getByProductBySearchTerm(searchTerm, pagenumber));
			model.addAttribute("totalPage", totalPage);
			model.addAttribute("currentPage", pagenumber);
			model.addAttribute("searchTerm", searchTerm);
			return "productsearch";
		} catch (Exception ex) {
			return "index";
		}
	}

}
