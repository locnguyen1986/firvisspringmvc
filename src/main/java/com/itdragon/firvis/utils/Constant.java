package com.itdragon.firvis.utils;

public class Constant {
	public static final String IMAGE_UPLOAD_LOCATION_PROPERTY_KEY = "image-location";
	public static final String EMAIL_RECEIVER_PROPERTY_KEY = "email-receiver";
}
