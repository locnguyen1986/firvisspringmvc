package com.itdragon.firvis.dao;

import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.itdragon.firvis.domain.Category;
import com.itdragon.firvis.domain.Product;

@Repository
public class CategoryDaoImpl implements CategoryDao {

	private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);

	@Autowired
	@Qualifier("mySessionFactory")
	private SessionFactory sessionFactory;

	@Autowired
	private ServletContext servletContext;

	@Transactional
	public Category create(Category category) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(category);
		tx.commit();
		session.close();
		return category;
	}

	@Transactional
	public void delete(Category category) {

		Session session = sessionFactory.openSession();
		List<Product> products = category.getProducts();
		if (products != null) {
			for (Product product : products) {
				product.removeCategory(category);
				session.update(product);
			}
		}
		session.delete(category);
		session.close();

	}

	@Transactional
	public Category update(Category category) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(category);
		tx.commit();
		session.close();
		return category;
	}

	@Transactional
	public Category getById(Integer id) {

		Session session = sessionFactory.openSession();
		Category category = (Category) session.get(Category.class, id);
		session.close();
		return category;
	}

	@Transactional
	public List<Category> getAll() {
		List<Category> categories = null;
		logger.info("-------------------------------------------------------------");
		logger.info("GET FROM DATABASE");
		Session session = sessionFactory.openSession();
		categories = session.createQuery("from Category c ORDER BY c.id ASC").list();
		session.close();

		return categories;
	}

	@Transactional
	public Category getByName(String categoryName) {

		Category result = null;

		/*
		 * List<Category> categories = getHibernateTemplate().findByNamedParam(
		 * "Select c from Category c where c.name = :name", "name",
		 * categoryName);
		 * 
		 * if (categories.size() == 1) { result = categories.get(0); }
		 * 
		 * if (categories.size() > 1) { throw new IllegalStateException(
		 * "More than one category found under name " + categoryName); }
		 */
		return result;
	}

	@Override
	public List<Category> getAll(Locale locale) {

		/*
		 * String sortBy = Category.getSortByField(locale); List<Category>
		 * result = getHibernateTemplate().find(
		 * "Select c from Category c order by " + sortBy);
		 */
		return null;
	}

}
