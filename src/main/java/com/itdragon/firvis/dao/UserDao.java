package com.itdragon.firvis.dao;

import com.itdragon.firvis.domain.FirvisUser;

public interface UserDao {
 
	FirvisUser findByUserName(String username);
 
}