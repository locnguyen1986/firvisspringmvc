package com.itdragon.firvis.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.itdragon.firvis.controller.ProductDetailController;
import com.itdragon.firvis.domain.ImageFile;
import com.itdragon.firvis.domain.Product;

@Repository
public class ProductDaoImpl implements ProductDao {

	private static final Logger logger = LoggerFactory.getLogger(ProductDetailController.class);
	public static final int PAGE_SIZE = 4;

	@Autowired
	@Qualifier("mySessionFactory")
	private SessionFactory sessionFactory;

	@Transactional
	public Product create(Product product) {
		Session session = sessionFactory.openSession();
		session.persist(product);
		session.flush();
		session.close();
		return product;
	}

	@Transactional
	public void delete(Product product) {

		deleteSmallImage(product);
		Session session = sessionFactory.openSession();

		session.delete(product);
		session.close();
	}

	@Transactional
	public Product update(Product product) {
		Session session = sessionFactory.openSession();
		logger.info("ProductDaoImpl.update.productId: " + product.getId());
		logger.info("ProductDaoImpl.update.productSmallImageName: " + product.getSmallImage().getFileName());
		logger.info("ProductDaoImpl.update.productSmallImageId: " + product.getSmallImage().getId());
		session.merge(product);
		logger.info("ProductDaoImpl.update.update" );
		session.flush();
		logger.info("ProductDaoImpl.update.refresh" );
		session.close();
		return product;
	}

	@Transactional
	public Product getById(final Integer id) {

		Product product = null;
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p left join fetch p.smallImage " +
		// "left join fetch p.categories " +
				"left join fetch p.images i where p.id = :id " + "order by i.orderNumber");
		query.setParameter("id", id);
		List<Product> products = query.list();

		if (products != null && !products.isEmpty()) {
			product = products.get(0);

			if (product.getImages() != null) {

				Iterator<ImageFile> it = product.getImages().iterator();
				while (it.hasNext()) {

					if (it.next().isSmallImage()) {
						it.remove();
					}
				}
			}
		}
		session.close();
		return product;

		/*
		 * HibernateCallback callback = new HibernateCallback() { public Object
		 * doInHibernate(Session session) { Product product = (Product)
		 * session.get(Product.class, id); if (product != null &&
		 * product.getImages() != null) {
		 * 
		 * Iterator<ImageFile> it = product.getImages().iterator(); while
		 * (it.hasNext()) {
		 * 
		 * if (it.next().isSmallImage()) { it.remove(); } } }
		 * 
		 * return product; } }; return (Product)
		 * getHibernateTemplate().execute(callback);
		 */
	}

	@Transactional
	public List<Product> getAll() {

		Session session = sessionFactory.openSession();
		List<Product> listProducts = session.createQuery("from Product").list();
		session.close();

		return listProducts;
	}

	@Transactional
	public Product getByCode(String code) {

		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p where p.code = :code");
		query.setParameter("code", code);
		List<Product> products = query.list();
		if (products != null && !products.isEmpty()) {
			return products.get(0);
		}
		session.close();
		return null;
	}

	@Transactional
	public List<Product> getByCategoryId(Integer categoryId) {

		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p left join fetch p.smallImage, " + "IN(p.categories) c where c.id = :categoryId");
		query.setParameter("categoryId", categoryId);
		List<Product> products = query.list();
		session.close();
		return products;
	}

	@Transactional
	public List<Product> getByCategoryId(Integer categoryId, Integer pageNumber) {

		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p left join fetch p.smallImage, " + "IN(p.categories) c where c.id = :categoryId ORDER BY p.id ASC");
		query.setParameter("categoryId", categoryId);
		List<Product> products = query.list();
		if (PAGE_SIZE * pageNumber > products.size())
			pageNumber = 0;
		int fromNumber = PAGE_SIZE * pageNumber;
		int toNumber = PAGE_SIZE * pageNumber + PAGE_SIZE;
		if (toNumber > products.size())
			toNumber = products.size();
		logger.info("getByCategoryId.fromNumber:" + fromNumber);
		logger.info("getByCategoryId.toNumber:" + toNumber);
		List<Product> returnProduct = products.subList(fromNumber, toNumber);
		session.close();
		return returnProduct;
	}

	public List<Product> getByProductBySearchTerm(String searchTerm) {
		logger.info("getByProductBySearchTerm:" + searchTerm);
		int categoryId = 1;
		String nativeSql = "SELECT * FROM Product  WHERE MATCH (Name,Content,Description) AGAINST ('+" + searchTerm + "'  IN BOOLEAN MODE)";
		logger.info("nativeSql:" + nativeSql);
		Session session = sessionFactory.openSession();
		SQLQuery query = session.createSQLQuery(nativeSql);
		query.addEntity(Product.class);
		List<Product> products = query.list();
		session.close();
		return products;
	}

	public List<Product> getByProductBySearchTerm(String searchTerm, Integer pageNumber) {
		int categoryId = 1;
		Session session = sessionFactory.openSession();
		String nativeSql = "SELECT * FROM Product  WHERE MATCH (Name,Content,Description) AGAINST ('+" + searchTerm + "'  IN BOOLEAN MODE)";
		logger.info("nativeSql:" + nativeSql);
		SQLQuery query = session.createSQLQuery(nativeSql);
		query.addEntity(Product.class);
		List<Product> products = query.list();
		if (PAGE_SIZE * pageNumber > products.size())
			pageNumber = 0;
		int fromNumber = PAGE_SIZE * pageNumber;
		int toNumber = PAGE_SIZE * pageNumber + PAGE_SIZE;
		if (toNumber > products.size())
			toNumber = products.size();
		logger.info("getByCategoryId.fromNumber:" + fromNumber);
		logger.info("getByCategoryId.toNumber:" + toNumber);
		List<Product> returnProduct = products.subList(fromNumber, toNumber);
		session.close();
		return returnProduct;
	}

	@Transactional
	public List<Product> getByCategoryId(Integer categoryId, Locale locale) {

		String orderBy = Product.getSortByField(locale);
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p left join fetch p.smallImage, " + "IN(p.categories) c where c.id = :categoryId order by p." + orderBy);
		query.setParameter("categoryId", categoryId);
		List<Product> products = query.list();
		session.close();
		return products;
	}

	@Transactional
	public void deleteSmallImage(Product product) {

		ImageFile smallImage = product.getSmallImage();
		if (smallImage != null) {
			product.setSmallImage(null);
			Session session = sessionFactory.getCurrentSession();

			session.update(product);
			session.delete(smallImage);

		}
	}

	@Override
	public void deleteImageByOrderNumber(Product product, int orderNumber) {

		ImageFile img = product.removeImageByOrderNumber(orderNumber);

		if (img != null) {
			product.setSmallImage(null);
			Session session = sessionFactory.getCurrentSession();

			session.delete(img);
			session.update(product);
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getUnassignedProducts() {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select p from Product p left join fetch p.smallImage where not exists " + "(select c from Category c, IN(c.products) p2 where p2 = p)");
		List<Product> products = query.list();
		session.close();
		return products;

	}

}
