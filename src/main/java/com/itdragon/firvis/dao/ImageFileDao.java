package com.itdragon.firvis.dao;

import java.util.List;

import com.itdragon.firvis.domain.ImageFile;

public interface ImageFileDao {

	ImageFile create(ImageFile imageFile);

	void delete(ImageFile imageFile);

	ImageFile update(ImageFile imageFile);

	ImageFile getById(Integer id);

	List<ImageFile> getAll();
	
	byte[] getImageContentById(Integer id);

}