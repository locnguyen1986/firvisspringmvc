package com.itdragon.firvis.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.itdragon.firvis.domain.FirvisUser;
 
@Repository
public class UserDaoImpl implements UserDao {
 
	@Autowired
	private SessionFactory sessionFactory;
 
	@SuppressWarnings("unchecked")
	public FirvisUser findByUserName(String username) {
 
		List<FirvisUser> users = new ArrayList<FirvisUser>();
 
		users = sessionFactory.getCurrentSession()
			.createQuery("from FirvisUser where username=?")
			.setParameter(0, username)
			.list();
 
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
 
	}
 
}