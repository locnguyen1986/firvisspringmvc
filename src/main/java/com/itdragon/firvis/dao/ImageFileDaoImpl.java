package com.itdragon.firvis.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.itdragon.firvis.controller.AboutUsController;
import com.itdragon.firvis.domain.ImageFile;

@Repository
public class ImageFileDaoImpl implements ImageFileDao {

	private static final Logger logger = LoggerFactory.getLogger(ImageFileDaoImpl.class);
	
	@Autowired
	@Qualifier("mySessionFactory")
	private SessionFactory sessionFactory;

	@Transactional
	public ImageFile create(ImageFile imageFile) {
		Session session = sessionFactory.openSession();

		session.persist(imageFile);
		session.flush();
		session.close();

		return imageFile;
	}

	@Transactional
	public void delete(ImageFile imageFile) {
		Session session = sessionFactory.getCurrentSession();

		session.delete(imageFile);

	}

	@Transactional
	public ImageFile update(ImageFile imageFile) {
		Session session = sessionFactory.getCurrentSession();

		session.update(imageFile);

		return imageFile;
	}

	@Transactional
	public ImageFile getById(Integer id) {
		logger.info("ImageFileDao getById");
		Session session = sessionFactory.openSession();
		logger.info("imageFile load" + session);
		ImageFile imageFile = (ImageFile) session.load(ImageFile.class, id);
		logger.info("imageFile return" + session);
		session.close();
		return imageFile;
	}

	@Transactional
	public byte[] getImageContentById(Integer id) {
		byte[] imageFileContent;
		Session session = sessionFactory.openSession();
		ImageFile imageFile = (ImageFile) session.load(ImageFile.class, id);
		imageFileContent = imageFile.getContent();
		session.close();
		return imageFileContent;
	}

	@Transactional
	public List<ImageFile> getAll() {
		Session session = sessionFactory.getCurrentSession();
		List<ImageFile> listImages = session.createQuery("from ImageFile").list();
		return listImages;
	}

}
