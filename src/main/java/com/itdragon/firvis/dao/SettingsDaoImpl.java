package com.itdragon.firvis.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.itdragon.firvis.controller.ProductDetailController;
import com.itdragon.firvis.domain.ImageFile;
import com.itdragon.firvis.domain.Product;
import com.itdragon.firvis.domain.Settings;

@Repository
public class SettingsDaoImpl implements SettingsDao {

	private static final Logger logger = LoggerFactory.getLogger(ProductDetailController.class);
	public static final int PAGE_SIZE = 4;

	@Autowired
	@Qualifier("mySessionFactory")
	private SessionFactory sessionFactory;

	@Transactional
	public Product create(Product product) {
		Session session = sessionFactory.openSession();
		session.persist(product);
		session.flush();
		session.close();
		return product;
	}
	@Transactional
	public List<Settings> getAllSettings() {
		Session session = sessionFactory.openSession();
		List<Settings> listSetting = session.createQuery("from Settings").list();
		session.close();
		return listSetting;
	}
	
	public Settings getSetting(String settingName){
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select s from Settings s where s.name = :name");
		query.setParameter("name", settingName);
		List<Settings> listSetting = query.list();
		session.close();
		if (listSetting != null && !listSetting.isEmpty()) {
			return listSetting.get(0);
		}

		return null;
		
	}

}
