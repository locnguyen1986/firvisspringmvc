package com.itdragon.firvis.dao;

import java.util.List;

import com.itdragon.firvis.domain.Settings;


public interface SettingsDao {
	public List<Settings> getAllSettings();
	public Settings getSetting(String settingName);	

}