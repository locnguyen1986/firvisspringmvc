﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
	$(document).ready(function() {
		$('#searchForm').bootstrapValidator();
	});
</script>


</head>
<body>
	<div id="page">
		<!-- Header -->
		<div id="header">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<h1>
							<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
							</a>
						</h1>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h1 class="h1-red">Fire Fighting with Technology</h1>
						<h4>FIRVIS is a leading provider of quality equipment for fire protection systems.</h4>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<!-- MAIN PAGE -->
		<div id="container">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
						<jsp:include page="/WEB-INF/includes/address.jsp" />
					</div>
				</div>
				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBannerAboutUs.png" alt="Firvis" />
						<form name="SendEmailAction" method="post" action="SendEmailAction.do" id="sendMail">
							<div class="banner-container">
								<div class="col-md-8">
									<div class="about-us-title">Introduction to Firvis</div>
									<div class="about-us-content">
										Hello, valuable world customer.<br> Firvis Co.,Ltd is a leading global provider of Quality equipment for fire fighting equipment and was founded
										in the early 2010’s, is a professional equipment.<br> Our company is located in Seoul City and our products have enjoyed a high reputation in the
										international market, selling well throughout 15 provinces in Korea as well as Middle Asia, Europe and USA.
									</div>
									<div class="about-us-title">Main products</div>
									<div class="about-us-content">
										Sprinklers, Alarm check valve, deluge valve, Flexible hose for Sprinkler etc Butterfly Valve, OS&Y Gate Valve, Check Valve.<br> 
										<img src="images/FirvisAboutUsLogo1.png" alt="Firvis" /><br>
										“Firvis Co.,Ltd” is appointed as a sole distributor by “Paradise Industry” to sell Paradise’s “FESCO” branded fire-sprinkler, flexible hoses, Valves and other products to Viet Nam and Myanmar.<br><br>										
										<img src="images/FirvisAboutUsLogo3.png" alt="Firvis" /><br><br>
										“Firvis Co.,Ltd” is appointed as a sole distributor by ALEUM's products and related products in Vietnam and Myanmar.<br><br>
										
										We would like to sincerely cooperate with foreign customers to create a better future.<br> Thank you,
									</div>
									<div style="color: black;">Firvis Co.,Ltd</div>
								</div>
								<div class="col-md-4">
									<img src="images/FirvisAboutUsCompanyNew.png" class="img-responsive" alt="Firvis Abouts Us Company Image">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<div id="push"></div>

	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
