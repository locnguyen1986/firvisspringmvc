﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">

</head>
<body>
	<div id="page">
		<!-- Header -->
		<div id="header">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<h1>
							<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
							</a>
						</h1>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h1 class="h1-red">Fire Fighting with Technology</h1>
						<h4>FIRVIS is a leading provider of quality equipment for fire protection systems.</h4>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<!-- MENU BANNER -->
		<div id="banner">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBanner.png" alt="Firvis" />
						<div class="banner-container">
							<div class="banner-image">
								<img style="border: 1px solid; border-color: #dbdbdb" src="images/FirvisMainBanner.png" alt="Firvis" />
							</div>
							<div class="banner-text">
								Firvis will reach <br> greater heights toward the world<br> and a greater furture
							</div>
						</div>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>

			</div>
		</div>
		<!-- MAIN PAGE -->
		<div id="container">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<!-- ADDRESS -->
						<div style="margin-top: -80px;">
							<jsp:include page="/WEB-INF/includes/address.jsp" />
						</div>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h4>Fire Fighting with Technology</h4>
						<p id="mccont">Firvis’s products have been helping to protect people, property and the environ -ment from fire. Today It develops, manufactures and
							distributors wide range of fire fighting equipments. All members of Firvis will continuously make efforts to fulfill the vision of “ the world leading
							company in the fire fighting industry” based on a positive attitude and creative mind.</p>
					</div>
				</div>

				<div id="right">
					<div class="column-in">
						<h4>Chữa cháy với công nghệ</h4>
						<p id="rccont">
							Sản phẩm Firvis giúp bảo vệ con người, tài sản và mội trường khỏi lửa. Ngày nay sản phẩm được phát triển, sản xuất và phân phối rộng rãi trong ngành
							thiết bị chữa cháy. Tất cả thành... <a class="read-more" href="./Vietnam">Xem thêm</a>
						</p>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>

			</div>
		</div>
		<div id="push"></div>
	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
