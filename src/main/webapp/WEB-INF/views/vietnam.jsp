﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
	$(document).ready(function() {
		$('#searchForm').bootstrapValidator();
	});
</script>

</head>
<body>
	<div id="page">
		<!-- Header -->
		<div id="header">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<h1>
							<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
							</a>
						</h1>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h1 class="h1-red">Fire Fighting with Technology</h1>
						<h4>FIRVIS is a leading provider of quality equipment for fire protection systems.</h4>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<!-- MAIN PAGE -->
		<div id="container">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
						<jsp:include page="/WEB-INF/includes/address.jsp" />
					</div>
				</div>
				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBanner.png" alt="Firvis" />

						<div class="banner-container">
							<img src="images/FirvisVietnamImage.png" class="img-responsive" alt="Firvis Vietnam Image">
							<p class="vietnam-page-content">Sản phẩm Firvis giúp bảo vệ con người, tài sản và mội trường khỏi lửa. Ngày nay sản phẩm được phát triển, sản xuất và
								phân phối rộng rãi trong ngành thiết bị chữa cháy. Tất cả thành viên của Firvis sẽ tiếp tục nỗ lực để hoàn thành triển vọng của một công ty dẫn đầu thế
								giới về thiết bị chữa cháy dựa vào tháiđộ tích cực và tinh thần sáng tạo.</p>
							<p class="vietnam-page-title">Các dự án ở Việt Nam</p>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Dự án</th>
										<th>Sản phẩm</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1/ Keangnam - Landmark tower</td>
										<td>Sprinkler, Valve, Sprinkler Flexible hose 2008~2011</td>
									</tr>
									<tr>
										<td>2/ Hyundai construction - (BITEXCO Finalcial Center)</td>
										<td>Sprinkler Flexible hose 2009~2011</td>
									</tr>
									<tr>
										<td>3/ Posco - Splendora</td>
										<td>Sprinkler, Valves 2012</td>
									</tr>
									<tr>
										<td>4/ Kumho Asia</td>
										<td>Valves 2010~20115/ Vietnam Local buyer : Valves, Sprinkler Flexible hose. 2008</td>
									</tr>
									<tr>
										<td>5/ Lotte Mart 3, 4th</td>
										<td>Sprinkler, Valve, Sprinkler Flexible hose. 2012.08~2012.12</td>
									</tr>
									<tr>
										<td>6/ Nokia</td>
										<td>Flexible hose, Flush sprinkler 2012.11~2013.05</td>
									</tr>
									<tr>
										<td>7/ Lotte Mart 5,6th</td>
										<td>Sprinkler, Valve, Sprinkler Flexible hose. 2013.08~2013.09</td>
									</tr>
									<tr>
										<td>8/ Hanoi Airport</td>
										<td>Flexible hose. 2013.09~10</td>
									</tr>
									<tr>
										<td>9/ LG Haiphong</td>
										<td>Sprinkler, Valve, Sprinkler Flexible hose 2014.03 ~ 2014.08.</td>
									</tr>
									<tr>
										<td>10/ SEVT-METAL</td>
										<td>Sprinkler, Valve, Flexible hose, etc 2014.10~2015.07.</td>
									</tr>
									<tr>
										<td>11/ Lotte Mart Go Vap</td>
										<td> Sprinkler, Valve, Flexible hose, Gas system, etc. 2015.09~2016.03.</td>
									</tr>
									<tr>
										<td>12/ SDV V2</td>
										<td>SFT, Sprinkler, Flexible hose, Valve etc. 2015.11~2016.06</td>
									</tr>
									<tr>
										<td>13/ Lotte Mart Nha trang</td>
										<td>Sprinkler, Valve, Flexible hose, etc. 2016.02~2016.08.</td>
									</tr>								
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<div id="push"></div>

	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
