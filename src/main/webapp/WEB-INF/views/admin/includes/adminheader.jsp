
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="/logout" var="logoutUrl" />
<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<script>
	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>
<!-- header logo: style can be found in header.less -->
<header class="header">
	<a href="index.html" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining --> AdminLTE
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</a>
		<div class="navbar-right" style="padding: 8px;">
			<a href="javascript:formSubmit()" class="btn btn-default btn-flat">Sign out</a>
		</div>
	</nav>

</header>