<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="java.util.List"%>
<c:set var="baseURL" value="${pageContext.request.contextPath}" />
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
		<li><a href="${baseURL}/admin/category/list"> <i class="fa fa-envelope"></i> <span>Category Management</span>
		</a></li>
		<li><a href="${baseURL}/admin/product/list"> <i class="fa fa-envelope"></i> <span>Product Management</span>
		</a></li>
		<li><a href="${baseURL}/admin/user/list"> <i class="fa fa-envelope"></i> <span>User Management</span>
		</a></li>
	</ul>
</section>
<!-- /.sidebar -->