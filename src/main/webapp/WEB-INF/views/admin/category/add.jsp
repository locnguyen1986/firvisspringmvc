<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="com.itdragon.firvis.domain.Category"%>
<%@page import="com.itdragon.firvis.domain.Product"%>
<%@page import="java.util.List"%>
<c:set var="baseURL" value="${pageContext.request.contextPath}" />
<%@page session="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Category | Data Table</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="${baseURL}/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="${baseURL}/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="${baseURL}/admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<link href="${baseURL}/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="${baseURL}/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- HEADER -->
	<jsp:include page="/WEB-INF/views/admin/includes/adminheader.jsp" />

	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- MENU -->
			<jsp:include page="/WEB-INF/views/admin/includes/adminmenu.jsp" />
		</aside>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					CATEGORY <small>ADD NEW CATEGORY</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#"><i class="fa fa-dashboard"></i> Category</a></li>
					<li class="active">Add New</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="box">
					<div class="box-body table-responsive">
						<div class="box box-warning">
							<form method="POST" action="${baseURL}/admin/category/add" role="form">
								<div class="box-header">
									<h3 class="box-title">Add New Category</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<!-- Category Id 
									<div class="form-group">
										<label>Category Id</label> <input type="text" name="categoryId" value="" class="form-control" placeholder=" disabled="">
									</div>-->

									<!-- Category Name -->
									<div class="form-group">
										<label>Category Name</label> <input type="text" name="categoryName" value="" class="form-control">
									</div>

								</div>
								<!-- /.box-body -->
								<div class="box-footer">
									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save changes</button>
										<button type="button" class="btn">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


			</section>

		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- add new calendar event modal -->


	<!-- jQuery 2.0.2 -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="${baseURL}/admin/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="${baseURL}/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="${baseURL}/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="${baseURL}/admin/js/AdminLTE/app.js" type="text/javascript"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="${baseURL}/admin/js/AdminLTE/demo.js" type="text/javascript"></script>
	<!-- page script -->
</body>
</html>