<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="com.itdragon.firvis.domain.Category"%>
<%@page import="com.itdragon.firvis.domain.Product"%>
<%@page import="java.util.List"%>
<html>
<head>
<title>Spring MVC Form Handling</title>
<script type="text/javascript"
	src="http://www.unverse.net/whizzery/whizzywig.js"></script>

</head>
<%
	List<Category> categories = (List<Category>) request.getAttribute("categories");
%>
<body onload="whizzywig()">

	<h2>Product Information</h2>
	<form:form method="POST" action="${baseURL}/admin/category/edit"
		enctype="multipart/form-data">
		<table>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="code">Code</form:label></td>
				<td><form:input path="code" /></td>
			</tr>
			<tr>
				<td><form:label path="description">Description</form:label></td>
				<td><form:textarea path="description" rows="5" cols="80" /></td>
			</tr>
			<tr>
				<td><form:label path="content">Content</form:label></td>
				<td><form:textarea path="content" rows="5" cols="80" /></td>
			</tr>
			<tr>
				<td><label for=productImageUrl>Image (in JPEG format
						only):</label></td>
				<td><input type="text" name="productImageUrl" /></td>
			</tr>
			<tr>
				<td><label for="listCategories">Choose Category</label></td>
				<td><select id="listCategories" name="listCategories"
					multiple="multiple">
						<c:if test="<%=categories != null%>">
							<%
								for (Category category : categories) {
							%>
							<option value="<%=category.getId()%>"><%=category.getName()%></option>
							<%
								}
							%>
						</c:if>
				</select></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>