<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="com.itdragon.firvis.domain.Category"%>
<%@page import="com.itdragon.firvis.domain.Product"%>
<%@page import="java.util.List"%>

<%
	List<Category> categories = (List<Category>) request.getAttribute("categories");
	List<Product> products = (List<Product>) request.getAttribute("products");
	Integer totalPage = (Integer) request.getAttribute("totalPage");
	Integer currentPage = (Integer) request.getAttribute("currentPage");
	String searchTerm = (String) request.getAttribute("searchTerm");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
	$(document).ready(function() {
		$('#searchForm').bootstrapValidator();
	});
</script>

</head>
<body>

	<div id="page">
		<!-- HEADER -->
		<jsp:include page="/WEB-INF/includes/header.jsp" />
		<!-- MAIN PAGE -->

		<div id="container">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
						<jsp:include page="/WEB-INF/includes/address.jsp" />
					</div>
				</div>
				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBannerProduct.png" alt="Firvis" />
						<div class="banner-container">
							<!--LIST CATEGORY MENU -->
							<jsp:include page="/WEB-INF/includes/productmenu.jsp" />
							<c:if test="<%=products != null%>">
								<%
									for (Product product : products) {
								%>


								<div class="product-list-product">
									<div class="col-md-12" style="padding-top: 20px;">
										<div class="col-md-4">
											<a href="./SearchProduct-Detail?productId=<%=product.getId()%>" class="thumbnail"> <img
												src="/imagesDir/<%=product.getSmallImage().getFileName()%>.jpg" alt="<%=product.getName()%>">
											</a>
										</div>


										<div class="col-md-8">
											<p class="product-list-title"><%=product.getName()%></p>
											<p class="product-list-detail"><%=product.getDescription()%></p>
											<a href="./SearchProduct-Detail?productId=<%=product.getId()%>">
												<div class="product-list-button-top">
													<span>See more</span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<%
									}
								%>
							</c:if>
							<c:if test="<%=totalPage > 1%>">
								<div class="product-pagination">
									<div class="col-md-12" style="text-align: center;">
										<ul class="pagination">
											<li><a href="./SearchProduct?searchTerm=<%=searchTerm%>&pagenumber=<%=currentPage > 1 ? currentPage - 1 : "0"%>">&laquo;</a></li>
											<c:if test="<%=totalPage != null%>">
												<%
												for (int i = 0; i < totalPage; i++) {
											%>
												<li><a <%=(i == currentPage ? "class='active'" : "")%> href="./SearchProduct?searchTerm=<%=searchTerm%>&pagenumber=<%=i%>"><%=i + 1%></a></li>
												<%
												}
											%>
											</c:if>
											<li><a href="./SearchProduct?searchTerm=<%=searchTerm%>&pagenumber=<%=currentPage < totalPage ? currentPage + 1 : totalPage%>">&raquo;</a></li>
										</ul>
									</div>
								</div>
							</c:if>
						</div>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<div id="push"></div>

	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
