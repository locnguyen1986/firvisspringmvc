<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="com.itdragon.firvis.domain.Category"%>
<%@page import="com.itdragon.firvis.domain.Product"%>
<%@page import="java.util.List"%>

<%
	Product product = (Product) request.getAttribute("product");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
	$(document).ready(function() {
		$('#searchForm').bootstrapValidator();
	});
</script>

</head>
<body>
	<div id="page">
		<!-- Header -->
		<div id="header">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<h1>
							<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
							</a>
						</h1>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h1 class="h1-red">Fire Fighting with Technology</h1>
						<h4>FIRVIS is a leading provider of quality equipment for fire protection systems.</h4>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<!-- MAIN PAGE -->
		<div id="container">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
						<jsp:include page="/WEB-INF/includes/address.jsp" />
					</div>
				</div>
				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBannerProduct.png" alt="Firvis" />
						<div class="banner-container">
							<!--LIST CATEGORY MENU -->
							<jsp:include page="/WEB-INF/includes/productmenu.jsp" />

							<div class="product-detail">
								<div class="col-md-12" style="padding-top: 20px;">
									<div class="col-md-4">
										<a href="./Product-Detail?productId=<%=product.getId()%>" class="thumbnail"> <img src="/imagesDir/<%=product.getSmallImage().getFileName()%>.jpg"
											alt="<%=product.getName()%>">
										</a>
									</div>
									<div class="col-md-8">
										<div class="col-md-12">
											<p class="product-detail-title"><%=product.getName()%></p>
										</div>
										<div class="col-md-12">
											<a href="./ContactUs" style="float: left; padding-right: 15px;">
												<div class="product-list-button-top">
													<span style="font-size: 9px;">Certification Data</span>
												</div>
											</a> <a href="./ContactUs" style="float: left;">
												<div class="product-list-button-top">
													<span style="font-size: 9px;">Customer Purchase Inquiry</span>
												</div>
											</a>
										</div>
										<div class="col-md-12">
											<p class="product-detail-detail"><%=product.getContent()%></p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<div id="push"></div>

	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
