<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/angular-xeditable/xeditable.css">
<link rel="stylesheet" type="text/css" href="css/custom/firvis.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script>
	$(document).ready(function() {
		$('#sendMail').bootstrapValidator();
	});
</script>
</head>
<body>
	<div id="page">
		<!-- Header -->
		<div id="header">
			<div id="row">

				<div id="left">
					<div class="column-in">
						<h1>
							<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
							</a>
						</h1>
					</div>
				</div>

				<div id="middle">
					<div class="column-in">
						<h1 class="h1-red">Fire Fighting with Technology</h1>
						<h4>FIRVIS is a leading provider of quality equipment for fire protection systems.</h4>

					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<!-- MAIN PAGE -->
		<div id="container">
			<div id="row">
				<div id="left">
					<div class="column-in">
						<!-- MENU -->
						<jsp:include page="/WEB-INF/includes/menu.jsp" />
						<!--<jsp:include page="/WEB-INF/includes/address.jsp" />-->
					</div>
				</div>
				<div id="middle">
					<div class="column-in">
						<img src="images/FirvisUpperBannerContactUs.png" alt="Firvis" />
						<form name="SendEmailAction" method="post" action="SendEmail" id="sendMail" data-bv-message="This value is not valid"
							data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
							data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
							<c:if test='<%= request.getAttribute("isSent") != null%>'>
								<c:if test='<%= "true".equals(request.getAttribute("isSent"))%>'>
									<div class="alert alert-success">Your message had been sent.</div>
								</c:if>
								<c:if test='<%= "false".equals(request.getAttribute("isSent"))%>'>
									<div class="alert alert-danger">Your message can't be sent . Please try again !!!</div>
								</c:if>
							</c:if>
							<div class="banner-container">

								<div class="col-md-8">
									<div class="col-md-6">
										<div class="form-group">
											<label for="firstname">FIRST NAME</label> <input type="text" class="form-control" name="firstname" placeholder="First name" data-bv-notempty="true"
												data-bv-notempty-message="The first name is required and cannot be empty" />
										</div>
										<div class="form-group">
											<label for="email">EMAIL</label> <input class="form-control" name="email" type="email" data-bv-emailaddress="true" placeholder="Email"
												data-bv-emailaddress-message="The input is not a valid email address" />

										</div>
										<div class="form-group">
											<label for="subject">SUBJECT</label> <input type="text" class="form-control" name="subject" placeholder="Subject" data-bv-notempty="true"
												data-bv-notempty-message="The subject is required and cannot be empty" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="lastname">LAST NAME</label> <input type="text" class="form-control" name="lastname" placeholder="Last name" data-bv-notempty="true"
												data-bv-notempty-message="The last name is required and cannot be empty" />
										</div>
										<div class="form-group">
											<label for="address">ADDRESS</label><input type="text" class="form-control" name="address" placeholder="Address" data-bv-notempty="true"
												data-bv-notempty-message="The address is required and cannot be empty" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="message">MESSAGE</label>
											<textarea name="message" class="form-control" rows="5" data-bv-notempty="true" data-bv-notempty-message="The message is required and cannot be empty"></textarea>
										</div>
										<input class="sendContactFormButton" type="submit" value="">
									</div>
								</div>
								<div class="col-md-4">
									<div class="contact-table">
										<address>
											<strong>CONTACT US</strong><br> For further inquiries, help or suggestions. You may contact us through the following:<br>
										</address>
										<address>
											<abbr title="Phone"><span class="glyphicon glyphicon-phone-alt"></span></abbr>&nbsp;&nbsp;&nbsp;82.2.2068.2721<br> <abbr title="Fax">Fax</abbr>&nbsp;&nbsp;&nbsp;82.2.6499.2721<br>
										</address>
										<address>
											<abbr title="Mail"><span class="glyphicon glyphicon-envelope"></span></abbr><a href="mailto:jmkim@firvis.com">&nbsp;&nbsp;&nbsp;jmkim@firvis.com</a>
										</address>
										<address>
											<abbr title="Home"><span class="glyphicon glyphicon-home"></span></abbr>&nbsp;&nbsp;&nbsp;Yangpyeong-ro, Yeongdeungpo-gu , 150-984, Seoul, Korea<br>
										</address>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="cleaner">&nbsp;</div>
			</div>
		</div>
		<div id="push"></div>

	</div>
	<!-- FOOTER -->
	<jsp:include page="/WEB-INF/includes/footer.jsp" />
</body>
</html>
