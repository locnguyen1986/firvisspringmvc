<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="com.itdragon.firvis.domain.Category"%>
<%@page import="com.itdragon.firvis.domain.Product"%>
<%@page import="java.util.List"%>
<%
	List<Category> categories = (List<Category>) request.getAttribute("categories");
%>

<div class="col-md-12">
	<c:if test="<%=categories != null%>">
		<%
			for (Category category : categories) {
		%>
		<div class="inline">
			<a href="./Product?categoryId=<%=category.getId()%>">
				<div class="product-list-button-top">
					<span><%=category.getLocalizedName(RequestContextUtils.getLocale(request))%></span>
				</div>
			</a>
		</div>
		<%
			}
		%>
	</c:if>
	<br class="clearBoth" />
</div>