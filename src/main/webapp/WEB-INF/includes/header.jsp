<!-- Header -->
<div id="header">
	<div id="row">

		<div id="left">
			<div class="column-in">
				<h1>
					<a href=""> <img src="images/FirvisLogo.png" alt="Firvis" />
					</a>
				</h1>
			</div>
		</div>

		<div id="middle">
			<div class="column-in">
				<h1 class="h1-red">Fire Fighting with Technology</h1>
				<h4>FIRVIS is a leading provider of quality equipment for fire
					protection systems.</h4>

			</div>
		</div>
		<div class="cleaner">&nbsp;</div>
	</div>
</div>