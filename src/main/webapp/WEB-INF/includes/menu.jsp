<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	
	String searchError =  null;
	if(request.getAttribute("searchError")!=null)
		searchError = request.getAttribute("searchError").toString();
%>
<img src="images/FirvisUpperLine.png" alt="Firvis" />
<div id="menu" style="text-color: black;">
	<a href="./Homepage" target="_self">Home Page</a><br> <a
		href="./AboutUs" target="_self">About Us</a><br> <a
		href="./Product" target="_self">Products</a><br> <a
		href="./ContactUs" target="_self">Contact Us</a><br>
</div>
<img src="images/FirvisUpperLine.png" alt="Firvis" />
<div class="input-group">
	<form name="SearchProduct" method="get" action="SearchProduct"
		id="searchForm" >
		<c:if test="<%=searchError != null && !searchError.isEmpty()%>">
			<span style="color:red"><%=searchError %></span>
		</c:if>
		<input type="text" class="form-control"
			style="height: 28px; float: left;width: 163px;" placeholder="Search Product"
			name="searchTerm" id="searchTerm" >
		<div class="input-group-btn" style="height: 28px; float: left;">
			<button class="btn btn-default" type="submit">
				<i class="glyphicon glyphicon-search"></i>
			</button>
		</div>
	</form>
</div>